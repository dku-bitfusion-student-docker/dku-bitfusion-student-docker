# Build the base Ubuntu 16.04 + some common libraries that will allow
# an interactive install of the bitfusion flexdirect client. The interactive
# install is necessary for now because I don't see a good way of scripting 
# responses to the prompts. So, build this image, then run it and do a
# dance to install the flexdirect client which can then be saved as a docker 
# image like this:
#
# 1.) Run a bash shell inside a container running the image we build here
#       sudo docker run --name bf-install -it ubuntu-bitfusion
#
# 2.) From the bash shell, folloow the Bitfusion directions to install 
# the flexdirect client
#
# 3.) Exit the shell. There will be a dead container - find it with the command:
#        sudo docker ps -a
# then commit the changes found in the dead container with this command:
#        sudo docker commit -m="Ubuntu 16.04 with Flexdirect client" 5da4d4af1a72
#
# 4.) Find the new untagged image with the command 
#        sudo docker images
# then tag the image using this command:
#        sudo docker tag 1ac2779f1c66 ubuntu16.04-flexdirect
#
#
FROM ubuntu:16.04
LABEL maintainer="Mark McCahill <mark.mccahill@duke.edu>"

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

USER root

# use local repositories 
RUN sed -i 's/archive.ubuntu.com/archive.linux.duke.edu/' /etc/apt/sources.list
RUN sed -i 's/security.ubuntu.com/archive.linux.duke.edu/' /etc/apt/sources.list


RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    ca-certificates 

# Install all OS dependencies for notebook server that starts but lacks all
# features (e.g., download as all possible file formats)
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    software-properties-common \
    wget \
    pwgen \
    sudo \
    net-tools \
    build-essential \
    libpng-dev \
    zlib1g-dev \
    libjpeg-dev \
    python-dev \
    python-pip \
    git \
    bzip2 \
    locales \
    vim \
    jed \
    unzip \
    libsm6 \
    locales && apt-get clean && rm -rf /var/lib/apt/lists/*
	
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    fonts-liberation \
    emacs \
    inkscape \
    jed \
    libsm6 \
    libxext-dev \
    libxrender1 \
    lmodern \
    netcat \
    pandoc \
    python-dev \
    texlive-fonts-extra \
    texlive-fonts-recommended \
    texlive-generic-recommended \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-xetex \
    unzip \
    nano \
    krb5-multidev \
    python-setuptools \
    pkg-config \
    apt-transport-https \
    lsb-core \
    gfortran \
    libopenblas-dev \
    liblapack-dev \
    ffmpeg && apt-get clean && rm -rf /var/lib/apt/lists/*
 
RUN locale-gen en_US.UTF-8
RUN update-locale LANG=en_US.UTF-8

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
  software-properties-common \
  python3-software-properties \
  apt-utils \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

#
# NVidia drivers
#
RUN add-apt-repository ppa:graphics-drivers

RUN apt-get update && apt-get install -y \
  nvidia-430 \
 && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN apt-get update &&  apt-get install -y \
    uuid \
    libjson-c2 \
    librdmacm1 \
    libprocps4 \
    procps \
   && apt-get clean && rm -rf /var/lib/apt/lists/* 

#
# CUDA toolkit
#
RUN wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
RUN dpkg -i cuda-repo-ubuntu1604_9.1.85-1_amd64.deb
RUN apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
RUN apt-get update &&  apt-get install -y \
      cuda-toolkit-9-1
RUN rm cuda-repo-ubuntu1604_9.1.85-1_amd64.deb

#
# some libraries for flexdirect 
#
RUN apt-get update &&  apt-get install -y \
    libjsoncpp1 \
    libssl-dev \
    libcapstone3 \
    libnl-route-3-200 \
   && apt-get clean && rm -rf /var/lib/apt/lists/*
